# Challenge API

This is just a simple example of how you can expose an API endpoint using Nginx (`ngnix.conf`) which passes the request to a Lua script (`api.lua`). 

## Examples
### POST
````
curl --location --request POST 'localhost/api/test' \
--header 'Content-Type: application/json' \
--data-raw '{
	"userName":"A",
    "bucketName":"arvanTest"
}'
````
### Returns (json)
````
{"body":{"example":"true","keyData":{}}}
````

### GET
````
curl --location --request GET 'localhost/api/test/1/A' \
--header 'Content-Type: application/json' \
--data-raw '{
	"example" : "true"
}'
````
## Returns (json)
````
{
    "body": {
        "example": "true",
        "keyData": {
            "userName": "A",
            "id": "1"
        }
    }
}
````

## Notice

you need to set the absolute path to the lua file in `content_by_lua_file` 

````
// ./api.conf
...
location ~ ^/api(.*)$ {
    default_type 'text/json';
    add_header 'Content-Type' 'application/json';

    ## NOTICE : change to this absolute path of lua file
    content_by_lua_file /ABS/PATH/TO/api.lua;
}
...
````