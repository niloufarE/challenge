local py = require 'python'
local http = require("socket.http")

local middleware = py.import "middleware".middleware

function strSplit(delim,str)
    local t = {}

    for substr in string.gmatch(str, "[^".. delim.. "]*") do
        if substr ~= nil and string.len(substr) > 0 then
            table.insert(t,substr)
        end
    end

    return t
end

-- Read body being passed
-- Required for ngx.req.get_body_data()
ngx.req.read_body();
-- Parser for sending JSON back to the client
local cjson = require("cjson")
local reqPath = ngx.var.uri:gsub("api/", "");
local reqMethod = ngx.var.request_method
-- Parse the body data as JSON
local body = ngx.req.get_body_data() == nil and {} or cjson.decode(ngx.req.get_body_data());

Api = {}
Api.__index = Api
Api.responded = false;
-- Function for checking input from client
function Api.endpoint(method, path, callback)
    -- If API not already responded
    if Api.responded == false then
        -- KeyData = params passed in path
        local keyData = {}
        -- If this endpoint has params
        if string.find(path, "<(.-)>")
        then
            local splitPath = strSplit("/", path)
            local splitReqPath = strSplit("/", reqPath)
            for i, k in pairs(splitPath) do
                if string.find(k, "<(.-)>")
                then
                    keyData[string.match(k, "%<(%a+)%>")] = splitReqPath[i]
                    reqPath = string.gsub(reqPath, splitReqPath[i], k)
                end
            end
        end

        if reqPath ~= path
        then
            return false;
        end
        if reqMethod ~= method
        then
            return ngx.say(
                cjson.encode({
                    error=500,
                    message="Method " .. reqMethod .. " not allowed"
                })
            )
        end
        Api.responded = true;
        body.keyData = keyData
        return callback(body);
    end

    return false;
end


Api.endpoint('POST', '/test',
    function(body)
        if body
        then
            local checkBucketName = middleware.__call__(body)
            if checkBucketName
            then
                local path = "https://s3.ir-thr-at1.arvanstorage.com/panel/bucket/"+body.bucketName"?is_public=true"
                local response_body = { }

                local res, code, response_headers, status = http.request{
                    url = path,
                    method = "POST",
                    headers =
                    {
                      ["Authorization"] = "865730be88b521da891182fa36ecefab6e25450fbc9c872361e84a242955b7d5",
                      ["Content-Type"] = "application/json"
                    },
                    sink = ltn12.sink.table(response_body)
                 }
                return ngx.say(
                    cjson.encode(
                        {
                            method=method,
                            path=path,
                            body= response_body
                            
                        }
                    )
                );
            end
            return ngx.say(
                cjson.encode(
                    {
                        status=false,
                        message='Bucket name is not valid!'
                    }
                )
            );
            
        end
        return ngx.say(
            cjson.encode(
                {
                    method=method,
                    path=path,
                    body=body       
                }
            )
        );
    end
)

Api.endpoint('GET', '/test/<userName>/<bucketName>',
    function(body)
        return ngx.say(
            cjson.encode(
                {
                    method=method,
                    path=path,
                    body=body,
                }
            )
        );
    end
)